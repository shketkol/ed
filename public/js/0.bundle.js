(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/bp-vuejs-dropdown/Dropdown.vue":
/*!*****************************************************!*\
  !*** ./node_modules/bp-vuejs-dropdown/Dropdown.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dropdown_vue_vue_type_template_id_42ca018e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dropdown.vue?vue&type=template&id=42ca018e& */ "./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=template&id=42ca018e&");
/* harmony import */ var _Dropdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dropdown.vue?vue&type=script&lang=js& */ "./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Dropdown_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Dropdown.vue?vue&type=style&index=0&lang=css& */ "./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Dropdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dropdown_vue_vue_type_template_id_42ca018e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dropdown_vue_vue_type_template_id_42ca018e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "node_modules/bp-vuejs-dropdown/Dropdown.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../vue-loader/lib??vue-loader-options!./Dropdown.vue?vue&type=script&lang=js& */ "./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _style_loader_index_js_css_loader_index_js_ref_5_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_5_2_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../style-loader!../css-loader??ref--5-1!../vue-loader/lib/loaders/stylePostLoader.js!../postcss-loader/src??ref--5-2!../vue-loader/lib??vue-loader-options!./Dropdown.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _style_loader_index_js_css_loader_index_js_ref_5_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_5_2_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_style_loader_index_js_css_loader_index_js_ref_5_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_5_2_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _style_loader_index_js_css_loader_index_js_ref_5_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_5_2_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _style_loader_index_js_css_loader_index_js_ref_5_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_5_2_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_style_loader_index_js_css_loader_index_js_ref_5_1_vue_loader_lib_loaders_stylePostLoader_js_postcss_loader_src_index_js_ref_5_2_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=template&id=42ca018e&":
/*!************************************************************************************!*\
  !*** ./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=template&id=42ca018e& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vue_loader_lib_loaders_templateLoader_js_vue_loader_options_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_template_id_42ca018e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../vue-loader/lib??vue-loader-options!./Dropdown.vue?vue&type=template&id=42ca018e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=template&id=42ca018e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _vue_loader_lib_loaders_templateLoader_js_vue_loader_options_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_template_id_42ca018e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _vue_loader_lib_loaders_templateLoader_js_vue_loader_options_vue_loader_lib_index_js_vue_loader_options_Dropdown_vue_vue_type_template_id_42ca018e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.bp-dropdown--sub {\n    width: 100%;\n}\n.bp-dropdown--sub .bp-dropdown__btn,\n.bp-dropdown--sub .bp-dropdown__sub {\n    width: 100%;\n}\n.bp-dropdown--sub .bp-dropdown__icon {\n    margin-left: auto;\n}\n.bp-dropdown__btn {\n    display: inline-flex;\n    align-items: center;\n    padding: 3px 5px;\n    border: 1px solid #efefef;\n    cursor: pointer;\n    transition: background-color .1s ease;\n}\n.bp-dropdown__sub {\n    display: inline-flex;\n    align-items: center;\n}\n.bp-dropdown__btn--active {\n    background-color: #eee;\n}\n.bp-dropdown__icon {\n    display: inline-block;\n    width: 15px;\n    height: 15px;\n    overflow: visible;\n    transition: transform .1s ease;\n}\n.bp-dropdown__icon--spin {\n    width: 12px;\n    height: 12px;\n    animation: spin 2s infinite linear;\n}\n.bp-dropdown__icon--top {\n    transform: rotate(-180deg);\n}\n.bp-dropdown__icon--right {\n    transform: rotate(-90deg);\n}\n.bp-dropdown__icon--bottom {\n    transform: rotate(0);\n}\n.bp-dropdown__icon--left {\n    transform: rotate(-270deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--top,\n.bp-dropdown__sub--active .bp-dropdown__icon--top {\n    transform: rotate(0);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--right,\n.bp-dropdown__sub--active .bp-dropdown__icon--right {\n    transform: rotate(-270deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--bottom,\n.bp-dropdown__sub--active .bp-dropdown__icon--bottom {\n    transform: rotate(-180deg);\n}\n.bp-dropdown__btn--active .bp-dropdown__icon--left,\n.bp-dropdown__sub--active .bp-dropdown__icon--left {\n    transform: rotate(-90deg);\n}\n.bp-dropdown__body {\n    position: fixed;\n    top: 0;\n    left: 0;\n    padding: 6px 8px;\n    background-color: #fff;\n    box-shadow: 0 5px 15px -5px rgba(0, 0, 0, .5);\n    z-index: 9999;\n}\n.fade-enter-active, .fade-leave-active {\n    transition: opacity .1s;\n}\n.fade-enter, .fade-leave-to {\n    opacity: 0;\n}\n@keyframes spin {\n0% {\n        transform:rotate(0)\n}\n100% {\n        transform:rotate(360deg)\n}\n}\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--5-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--5-2!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../css-loader??ref--5-1!../vue-loader/lib/loaders/stylePostLoader.js!../postcss-loader/src??ref--5-2!../vue-loader/lib??vue-loader-options!./Dropdown.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib??vue-loader-options!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'bp-vuejs-dropdown',

    props: {
        role: {
            type: String,
            required: false,
            default: ''
        },

        unscroll: {
            type: [HTMLElement, String],
            required: false,
            default: null
        },

        align: {
            type: String,
            required: false,
            default: 'bottom'
        },

        x: {
            type: Number,
            required: false,
            default: 0
        },

        y: {
            type: Number,
            required: false,
            default: 0
        },

        beforeOpen: {
            type: Function,
            required: false,
            default: resolve => resolve()
        },

        trigger: {
            type: String,
            required: false,
            default: 'click'
        },

        closeOnClick: {
            type: Boolean,
            required: false,
            default: false
        },

        isIcon: {
            type: Boolean,
            required: false,
            default: true
        },

        className: {
            type: String,
            required: false,
            default: ''
        },
    },

    data() {
        return {
            isHidden: true,
            isLoading: false,

            id: null,
            timeout: null,

            top: undefined,
            right: undefined,
            bottom: undefined,
            left: undefined,
            width: undefined
        }
    },

    watch: {
        isHidden(isHidden) {
            if (this.unscroll) {
                const el = (this.unscroll instanceof HTMLElement) ?
                    this.unscroll : document.querySelector(this.unscroll);

                if (el) {
                    el.style.overflow = (!isHidden) ? 'hidden' : '';
                }
            }
        }
    },

    created() {
        const $root = this.$root;

        // --- hide dropdown if other dropdowns show
        // --- or document clicked
        $root.$on('bp-dropdown:open', () => this.isHidden = true);
        $root.$on('bp-dropdown:hide', () => this.isHidden = true);

        // --- hide dropdown on document click event
        if (this.trigger === 'click' && !$root['is-bp-dropdown']) {
            Object.defineProperty($root, 'is-bp-dropdown', {
                enumerable: false,
                configurable: false,
                writable: false,
                value: true
            });

            document.onmousedown = (e) => {
                const target = e.target;
                const dropdown = target.closest('.bp-dropdown__btn') || target.closest('.bp-dropdown__body');

                if (!dropdown) {
                    $root.$emit('bp-dropdown:hide');
                }
            }
        }

        this.id = 'bp-dropdown-' + this.generateRandomId();
    },

    methods: {
        // --- generate random id for query selector
        generateRandomId() {
            return Math.random().toString(36).substr(2, 10);
        },

        _onToggle(e) {
            if (this.trigger !== 'click') {
                return;
            }

            this.checkCustomCallback(e);
        },

        _onBtnEnter(e) {
            if (this.trigger !== 'hover' || !this.isHidden) {
                return;
            }

            this.checkCustomCallback(e);
        },

        _onBtnLeave(e) {
            if (this.trigger !== 'hover') {
                return;
            }

            if (this.role) {
                this.timeout = setTimeout(() => this.isHidden = true, 100);
            }

            const to = e.toElement;
            if (!to) {
                return;
            }

            const isDropdown = to.closest('.bp-dropdown__btn') || to.closest('.bp-dropdown__body');
            if (isDropdown) {
                return;
            }

            this.prepare();
        },

        _onBodyClick() {
            if (this.closeOnClick) {
                this.isHidden = true;
            }
        },

        _onBodyEnter() {
            if (this.timeout) {
                clearTimeout(this.timeout);
            }
        },

        _onBodyLeave(e) {
            if (this.trigger !== 'hover') {
                return;
            }

            const to = e.toElement;
            if (!to) {
                return;
            }

            if (to.closest('.bp-dropdown__btn') || to.closest('.bp-dropdown__sub')) {
                return;
            }

            this.prepare();
        },

        checkCustomCallback(e) {
            if (!this.isHidden) {
                this.prepare();
                return;
            }

            // --- custom callback before open
            const promise = new Promise(resolve => {
                this.isLoading = true;
                this.beforeOpen.call(this, resolve);
            });

            promise.then(() => {
                this.isLoading = false;
                if (!e.target.closest('.bp-dropdown__body')) {
                    // --- hide dropdown if other dropdowns show
                    this.$root.$emit('bp-dropdown:open');
                }

                setTimeout(this.prepare, 0);
            });

            promise.catch(() => { throw Error('bp-dropdown promise error') });
        },

        prepare() {
            this.isHidden = !this.isHidden;
            if (!this.isHidden) {
                this.$nextTick(() => {
                    const button = this.$el.firstElementChild;
                    const container = document.getElementById(this.id);

                    this.setWidth(button.offsetWidth);
                    this.setPosition(button, container);
                });
            }
        },

        setWidth(width) {
            this.width = width;
        },

        setPosition(btn, body) {
            if (!btn || !body) {
                return;
            }

            const coords = this.getCoords(btn);

            // --- current position
            const currentTop = coords.top;
            const currentLeft = coords.left;

            // --- btn size
            const btnWidth = btn.offsetWidth;
            const btnHeight = btn.offsetHeight;

            // --- body size
            const bodyWidth = body.offsetWidth;
            const bodyHeight = body.offsetHeight;

            switch(this.align) {
                case 'top':
                    this.top = (currentTop + pageYOffset - bodyHeight);
                    this.left = (currentLeft + pageXOffset);
                    break;
                case 'right':
                    this.top = (currentTop + pageYOffset);
                    this.left = (currentLeft + pageXOffset + btnWidth);
                    break;
                case 'bottom':
                    this.top = (currentTop + pageYOffset + btnHeight);
                    this.left = (currentLeft + pageXOffset);
                    break;
                case 'left':
                    this.top = (currentTop + pageYOffset);
                    this.left = (currentLeft + pageXOffset - bodyWidth);
                    break;
                default:
                    this.top = (currentTop + pageYOffset + btnHeight);
                    this.left = (currentLeft + pageXOffset);
                    break;
            }

            this.top += this.y;
            this.left += this.x;
        },

        getCoords(el) {
            el = el.getBoundingClientRect();
            return {
                top: el.top - pageYOffset,
                left: el.left - pageXOffset
            };
        }
    }
});


/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=template&id=42ca018e&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./node_modules/bp-vuejs-dropdown/Dropdown.vue?vue&type=template&id=42ca018e& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "bp-dropdown",
      class: { className: _vm.className, "bp-dropdown--sub": _vm.role }
    },
    [
      _c(
        "span",
        {
          class: ((_obj = {}),
          (_obj["bp-dropdown__" + (_vm.role ? "sub" : "btn")] = true),
          (_obj[
            "bp-dropdown__" + (_vm.role ? "sub" : "btn") + "--active"
          ] = !_vm.isHidden),
          (_obj[_vm.className + "-bp__btn"] = _vm.className),
          (_obj[_vm.className + "-bp__btn--active"] = !_vm.isHidden),
          _obj),
          on: {
            click: _vm._onToggle,
            mouseenter: _vm._onBtnEnter,
            mouseleave: _vm._onBtnLeave
          }
        },
        [
          _vm._t("btn"),
          _vm._v(" "),
          _vm.isIcon
            ? _vm._t("icon", [
                _vm.isLoading
                  ? _c(
                      "svg",
                      {
                        staticClass:
                          "bp-dropdown__icon bp-dropdown__icon--spin",
                        attrs: { viewBox: "0 0 512 512" }
                      },
                      [
                        _c("path", {
                          attrs: {
                            fill: "currentColor",
                            d:
                              "M304 48c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-48 368c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zm208-208c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.49-48-48-48zM96 256c0-26.51-21.49-48-48-48S0 229.49 0 256s21.49 48 48 48 48-21.49 48-48zm12.922 99.078c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.491-48-48-48zm294.156 0c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48c0-26.509-21.49-48-48-48zM108.922 60.922c-26.51 0-48 21.49-48 48s21.49 48 48 48 48-21.49 48-48-21.491-48-48-48z"
                          }
                        })
                      ]
                    )
                  : _c(
                      "svg",
                      {
                        staticClass: "bp-dropdown__icon",
                        class: ((_obj$1 = {}),
                        (_obj$1["bp-dropdown__icon--" + _vm.align] = _vm.align),
                        _obj$1),
                        attrs: { viewBox: "0 0 256 512" }
                      },
                      [
                        _c("path", {
                          attrs: {
                            fill: "currentColor",
                            d:
                              "M119.5 326.9L3.5 209.1c-4.7-4.7-4.7-12.3 0-17l7.1-7.1c4.7-4.7 12.3-4.7 17 0L128 287.3l100.4-102.2c4.7-4.7 12.3-4.7 17 0l7.1 7.1c4.7 4.7 4.7 12.3 0 17L136.5 327c-4.7 4.6-12.3 4.6-17-.1z"
                          }
                        })
                      ]
                    )
              ])
            : _vm._e()
        ],
        2
      ),
      _vm._v(" "),
      _c("transition", { attrs: { name: "fade" } }, [
        !_vm.isHidden
          ? _c(
              "div",
              {
                staticClass: "bp-dropdown__body",
                class: ((_obj$2 = {}),
                (_obj$2[_vm.className + "-bp__body"] = _vm.className),
                _obj$2),
                style: {
                  minWidth: _vm.width + "px",
                  top: _vm.top + "px",
                  left: _vm.left + "px"
                },
                attrs: { id: _vm.id },
                on: {
                  click: _vm._onBodyClick,
                  mouseenter: _vm._onBodyEnter,
                  mouseleave: _vm._onBodyLeave
                }
              },
              [_vm._t("body")],
              2
            )
          : _vm._e()
      ])
    ],
    1
  )
  var _obj
  var _obj$1
  var _obj$2
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);
@extends('layouts.profile')

@section('content')
    <div class="page-search">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <app-search default-select="вакансии"></app-search>
                </div>
            </div>
        </div>
    </div>

    <section class="page search">
        <div class="container">
            <app-vacancy-search ></app-vacancy-search>
        </div>
    </section>
@endsection
